package twigger

import (
	"fmt"
	"log"
	"math"
	"net/url"
	"strconv"
	"unsafe"
)

func (c *Connection) GetRecentTweetsFromX(values url.Values) (Tweets, error){
	cli := c.Client
	maxID := math.MaxInt64 - 1
	maxIDStr := fmt.Sprintf("%v", maxID)
	values.Set("max_id", maxIDStr)
	allTweets := make([]Tweet, 0)

	i := 0

	for{
		i++
		log.Printf("Obtaining tweet set IND: %v", i)
		tweets, err := cli.GetUserTimeline(values)
		log.Printf("Obtained tweet set IND: %v", i)
		if err != nil{
			return allTweets, err
		}
		if len(tweets) == 0{
			break
		}

		maxID := tweets[len(tweets) -  1].Id - 1
		maxIDStr := fmt.Sprintf("%v", maxID)
		values.Set("max_id", maxIDStr)
		curTweets := *(*Tweets)(unsafe.Pointer(&tweets))
		allTweets = append(allTweets, curTweets...)
		if len(curTweets) == 0{
			log.Println("Never need to enter here")
			break
		}
	}
	return allTweets, nil
}

func (c *Connection) GetRecentTweetsFromScreenName(screenName string)(Tweets, error){
	values := url.Values{}
	values.Set("screen_name", screenName)
	values.Set("count", "200")
	return c.GetRecentTweetsFromX(values)
}

func (c *Connection) GetFirstNTweetsFromScreenName(screenName string, n int)(Tweets, error){
	cli := c.Client
	maxID := math.MaxInt64 - 1
	maxIDStr := fmt.Sprintf("%v", maxID)
	values := url.Values{}
	values.Set("count", "200")
	values.Set("max_id", maxIDStr)
	values.Set("screen_name", screenName)
	allTweets := make([]Tweet, 0)

	i := 0

	for{
		i++
		log.Printf("Obtaining tweet set IND: %v", i)
		tweets, err := cli.GetUserTimeline(values)
		log.Printf("Obtained tweet set IND: %v", i)
		if err != nil{
			return allTweets, err
		}
		if len(tweets) == 0{
			break
		}



		maxID := tweets[len(tweets) -  1].Id - 1
		maxIDStr := fmt.Sprintf("%v", maxID)
		values.Set("max_id", maxIDStr)
		curTweets := *(*Tweets)(unsafe.Pointer(&tweets))
		allTweets = append(allTweets, curTweets...)
		if len(allTweets) >= n{
			break
		}
		if len(curTweets) == 0{
			log.Println("Never need to enter here")
			break
		}
	}
	return allTweets[:n], nil
}

func (c *Connection) GetRecentFavoritesFromX(values url.Values) (Tweets, error){
	cli := c.Client
	maxID := math.MaxInt64 - 1
	maxIDStr := fmt.Sprintf("%v", maxID)
	values.Set("max_id", maxIDStr)
	allTweets := make([]Tweet, 0)

	i := 0

	for{
		i++
		log.Printf("Obtaining tweet set IND: %v", i)
		tweets, err := cli.GetFavorites(values)
		log.Printf("Obtained tweet set IND: %v", i)
		if err != nil{
			return allTweets, err
		}
		if len(tweets) == 0{
			break
		}

		maxID := tweets[len(tweets) -  1].Id - 1
		maxIDStr := fmt.Sprintf("%v", maxID)
		values.Set("max_id", maxIDStr)
		curTweets := *(*Tweets)(unsafe.Pointer(&tweets))
		allTweets = append(allTweets, curTweets...)
		if len(curTweets) == 0{
			log.Println("Never need to enter here")
			break
		}
	}
	return allTweets, nil
}

func (c *Connection) GetFavoritesFromScreenName(screenName string)(Tweets, error){
	values := url.Values{}
	values.Set("screen_name", screenName)
	values.Set("count", "200")
	return c.GetRecentFavoritesFromX(values)
}

func (c *Connection) GetAllMentionTweets()(Tweets, error){
	cli := c.Client
	v := url.Values{}
	allTweets := make([]Tweet, 0)
	for{
		anacondaTweets, err := cli.GetMentionsTimeline(v)
		if err != nil{
			return allTweets, err
		}
		if len(anacondaTweets) == 0{
			break
		}
		curTweets := *(*Tweets)(unsafe.Pointer(&anacondaTweets))
		allTweets = append(allTweets, curTweets...)

		newMaxID := fmt.Sprintf("%v", anacondaTweets[len(anacondaTweets) - 1].Id - 1)
		v.Set("max_id", newMaxID)
	}
	return allTweets, nil
}

func (c *Connection) GetAllMentionTweetsSince(sinceID int64)(Tweets, error){
	cli := c.Client
	v := url.Values{}
	v.Set("since_id", fmt.Sprintf("%v", sinceID))
	allTweets := make([]Tweet, 0)
	for{
		anacondaTweets, err := cli.GetMentionsTimeline(v)
		if err != nil{
			return allTweets, err
		}
		if len(anacondaTweets) == 0{
			break
		}
		curTweets := *(*Tweets)(unsafe.Pointer(&anacondaTweets))
		allTweets = append(allTweets, curTweets...)

		newMaxID := fmt.Sprintf("%v", anacondaTweets[len(anacondaTweets) - 1].Id - 1)
		v.Set("max_id", newMaxID)
	}
	return allTweets, nil
}



func (c *Connection) GetTimelineTweets()(Tweets, error){
	cli := c.Client
	v := url.Values{}
	v.Set("count", "200")
	allTweets := make([]Tweet, 0)
	for{
		anacondaTweets, err := cli.GetHomeTimeline(v)
		if err != nil{
			return allTweets, err
		}
		if len(anacondaTweets) == 0{
			break
		}
		curTweets := *(*Tweets)(unsafe.Pointer(&anacondaTweets))
		allTweets = append(allTweets, curTweets...)

		newMaxID := fmt.Sprintf("%v", anacondaTweets[len(anacondaTweets) - 1].Id - 1)
		v.Set("max_id", newMaxID)
	}
	return allTweets, nil
}

func (c *Connection) DeleteTweet(id string) (Tweet, error){
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil{
		return Tweet{}, err
	}
	tweet, err := c.Client.DeleteTweet(idInt, false)
	return Tweet(tweet), err
}

func (c *Connection) GetSingleTweetFromID(id int64)(Tweet, error){
	//IDStr := fmt.Sprintf("%v", id)
	tw, err := c.Client.GetTweet(id, nil)
	return Tweet(tw), err
}


