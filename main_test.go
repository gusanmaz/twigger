package twigger

import (
	"fmt"
	"gitlab.com/gusanmaz/twigger/genjson"
	"log"
	"net/url"
	"os"
	"testing"
	"time"
)

var loginCredentialsPath = "test_files/real_login.json"

var credentials Credentials
var connection  *Connection

func TestMain(m *testing.M){
	FakeMain()
}

func  FakeMain(){
	creds, err := LoadJSONCredentials("test_files/real_login.json")
	conn,  err := NewConnection(creds)
	if err != nil{
		log.Fatalln("Cannot continue")
	}

	//MentionsTest(conn)
	//SingleTweetTest(conn)
	DownloadTest(conn)
	//FriendTest(conn)
	//FollowerTest(conn)
	//UploadImageTest(conn)
	//UploadVideoTest(conn)
	//UserQueryTest(conn)
	//MentionsTest(conn)
	//TimelineTest(conn)
	//PostAndDelete(conn)
	//TweetSpecialTests(conn)
}

func FriendTest(c *Connection){
	friends, err := c.GetAllFriendsFromScreenName("AFKL33")
	if err != nil{
		os.Exit(-1)
	}
	fMap := friends.ToMap()
	fMap.Save("AFKL33_friends.json")
}

func FollowerTest(c *Connection){
	followers, err := c.GetAllFollowersFromScreenName("kirmizigokyuzu")
	if err != nil{
		os.Exit(-1)
	}
	fMap := followers.ToMap()
	fMap.Save("guvenc_followers.json")
}

func FavoritesTest(c *Connection){
	tweets, err := c.GetFavoritesFromScreenName("kirmizigokyuzu")
	if err != nil{
		log.Fatalf("Kirmizi Gokyuzu")
	}
	err = genjson.SaveDataTo(tweets, "guvenc_favs.json")
	if err != nil{
		log.Println("Cannot save the data")
	}
}

func QueryTest(c *Connection){
	vv := url.Values{}
	vv.Set("result_type", "mixed")
	vv.Set("count", "50")
	er, err := c.GetRecentTweetsByQuery("erdogan")
	if err != nil{
		os.Exit(-1)
	}
	fmt.Println(er)
}

func UserTweetsTest(c *Connection){

}

func UploadImageTest(c *Connection){
	c.PublishPictureTweet("test_files/pictures/antalya.jpg", "This is a test tweet with a picture!")
}

func UploadVideoTest(c *Connection){
	c.PublishVideoTweet("test_files/hp.mov", "This is a test tweet with a video!")
}

func UserQueryTest(c *Connection){
	users, err := c.GetTopUsersFromQuery("cnn")
	fmt.Println(users,err)
}

func MentionsTest(c *Connection){
	tweets, err := c.GetAllMentionTweets()
	fmt.Println(tweets,err)
}

func TimelineTest(c *Connection){
	tweets, err := c.GetTimelineTweets()
	fmt.Println(tweets,err)
}

func SingleTweetTest(c *Connection){
	id := int64(1370326420619165699)
	tweets, err := c.GetSingleTweetFromID(id)
	medias := tweets.GetMedias()
	fmt.Print(medias)
	fmt.Print(tweets, err)
}

func PostAndDelete(c *Connection){
	images := []string{"antalya.jpg", "ny.jpg", "providence.jpg", "eskisehir.jpg"}

	for  i, v := range images{
		images[i] = "test_files/pictures/" + v
	}
	id, err := c.PublishCollageTweet(images, "This is a test picture tweet generated by a bot and will be delete soon.")
	if err != nil{
		fmt.Println("error")
	}
	fmt.Println("Sleeping for 10 seconds")
	time.Sleep(time.Second * 10)
	_, err = c.DeleteTweet(id)
	if err != nil{
		fmt.Println("error 2")
	}
}

func TweetSpecialTests(c *Connection){
	tweets, err := c.GetFirstNTweetsFromScreenName("guardian", 300)
	if err != nil{
		fmt.Print("Error")
	}
	tweets.Save("Hello.json")

	for _, v := range tweets{
		urls := v.GetTextURLs()
		fmt.Print(urls)
	}

	specificTweet1 := tweets[0]
	//specificTweet2 := tweets[1]
	urls := specificTweet1.GetTextURLs()
	fmt.Println(urls)
}

func DownloadTest(c *Connection){
	id := int64(1370326420619165699)
	tw, _ := c.GetSingleTweetFromID(id)
	tw.DownloadMedia()
}


