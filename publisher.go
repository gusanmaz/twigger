package twigger

import (
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/gabriel-vasile/mimetype"
	"io/ioutil"
	"log"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

func (c *Connection) UploadImage(filepath string) (int64, error){
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return -1, err
	}

	mimeData, err := mimetype.DetectFile(filepath)
	if err != nil {
		return -1, err
	}
	acceptReg := regexp.MustCompile("(?i)jpeg|jpg|png|webp|gif")
	if !acceptReg.MatchString(mimeData.String()){
		return -1, errors.New("Twitter only accepts jpeg,png,webp,gif mime type image uploads.")
	}

	client := c.Client
	mediaResponse, err := client.UploadMedia(base64.StdEncoding.EncodeToString(data))
	if err != nil{
		return -1, err
	}
	return mediaResponse.MediaID, nil
}

func (c *Connection) UploadVideo(filepath string)(int64, error){
	bytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		return -1, err
	}

	mimeData, err := mimetype.DetectFile(filepath)
	if err != nil {
		return -1, err
	}
	acceptReg := regexp.MustCompile("quicktime|mp4")
	if !acceptReg.MatchString(mimeData.String()){
		return -1, errors.New("Twitter only accepts mp4 and quicktime mime type videos uploads.")
	}

	totalBytes := len(bytes)
	media, err := c.Client.UploadVideoInit(totalBytes, "video/quicktime")
	if err != nil {
		return -1, err
	}

	mediaMaxLen := 5 * 1024 * 1024
	segment := 0
	for i := 0; i < totalBytes; i += mediaMaxLen {
		var mediaData string
		if i+mediaMaxLen < totalBytes {
			mediaData = base64.StdEncoding.EncodeToString(bytes[i : i+mediaMaxLen])
		} else {
			mediaData = base64.StdEncoding.EncodeToString(bytes[i:])
		}
		if err = c.Client.UploadVideoAppend(media.MediaIDString, segment, mediaData); err != nil {
			break
		}
		segment += 1
	}
	if err != nil {
		return -1, err
	}

	video, err := c.Client.UploadVideoFinalize(media.MediaIDString)
	if err != nil {
		return -1, err
	}
	return video.MediaID, nil
}

func (c *Connection) PublishPictureTweet(filepath string, text string) (string, error){
	mediaID, err := c.UploadImage(filepath)
	if err != nil{
		return "-1", err
	}

	v := url.Values{}
	v.Set("media_ids", strconv.FormatInt(mediaID, 10))

	result, err := c.Client.PostTweet(text, v)

	_ = result
	if err != nil {
		return "-1", err
	}
	return result.IdStr, nil
}

func (c *Connection) PublishCollageTweet(filepaths []string, text string) (string, error){
	client := c.Client
	if len(filepaths) > 4{
		log.Println("You cannot picture more than 4 pictures!")
		filepaths = filepaths[:4]
	}

	mediaIDs := make([]string, len(filepaths))
	for i, filepath := range filepaths{
		mediaID, _ := c.UploadImage(filepath)
		mediaIDs[i] =  strconv.FormatInt(mediaID, 10)
	}

	v := url.Values{}
	v.Set("media_ids", strings.Join(mediaIDs, ","))

	result, err := client.PostTweet(text, v)
	if err != nil {
		return result.IdStr, err
	} else {
		fmt.Println(result)
		return result.IdStr, nil
	}
}

func (c *Connection) PublishPictureTweetTo(filepath string, text string, toID string) (string, error){
	mediaID, err := c.UploadImage(filepath)
	if err != nil{
		return "-1", err
	}

	v := url.Values{}
	v.Set("media_ids", strconv.FormatInt(mediaID, 10))
	v.Set("in_reply_to_status_id", toID)

	result, err := c.Client.PostTweet(text, v)

	_ = result
	if err != nil {
		return "-1", err
	}
	return result.IdStr, nil
}

func (c *Connection) PublishCollageTweetTo(filepaths []string, text string, toID string) (string, error){
	if c == nil{
		return "-1",errors.New("nil receiver")
	}
	client := c.Client
	if len(filepaths) > 4{
		log.Println("You cannot picture more than 4 pictures!")
		filepaths = filepaths[:4]
	}

	mediaIDs := make([]string, len(filepaths))
	for i, filepath := range filepaths{
		mediaID, _ := c.UploadImage(filepath)
		mediaIDs[i] =  strconv.FormatInt(mediaID, 10)
	}

	v := url.Values{}
	v.Set("media_ids", strings.Join(mediaIDs, ","))
	v.Set("in_reply_to_status_id", toID)

	result, err := client.PostTweet(text, v)
	if err != nil {
		return result.IdStr, err
	} else {
		fmt.Println(result)
		return result.IdStr, nil
	}
}



func (c *Connection) PublishVideoTweet(filepath string, text string) error{
	mediaID, err := c.UploadVideo(filepath)
	if err != nil{
		return err
	}

	v := url.Values{}
	v.Set("media_ids", strconv.FormatInt(mediaID, 10))

	result, err := c.Client.PostTweet(text, v)
	_ = result
	if err != nil {
		return err
	}
	return nil
}



