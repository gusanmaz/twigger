package twigger

import (
	"github.com/ChimeraCoder/anaconda"
)

type Connection struct{
	Client *anaconda.TwitterApi
}

func NewConnection(c Credentials) (*Connection, error){
	anacondaClient := anaconda.NewTwitterApiWithCredentials(c.AccessToken, c.AccessSecret,  c.APIKey, c.APISecret)
	_, err := anacondaClient.VerifyCredentials()
	return &Connection{anacondaClient}, err
}



