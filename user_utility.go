package twigger


func (u User) GetEmail()string{
	return u.Email
}

func (u User) GetProfileImageURL() string{
	return u.ProfileImageURL
}

func (u User) GetBackgroundImageURL() string{
	return u.ProfileBackgroundImageURL
}

func (u User) IsVerified() bool{
	return u.Verified
}

func (u User ) GetDescription() string{
	return u.Description
}

func (u User) GetScreenName() string{
	return u.ScreenName
}

func (u User) GetID() string{
	return u.IdStr
}

func (u User) IsProtected() bool{
	return u.Protected
}

func (u User) GetStatusCount() int64{
	return u.StatusesCount
}

func (u User) GetFavoritesCount() int{
	return u.FavouritesCount
}

func (u User) GetFollowingCount() int{
	return u.FriendsCount
}

func (u User) GetFollowersCount() int{
	return u.FollowersCount
}



