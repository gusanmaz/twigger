package twigger

import (
	"fmt"
	"github.com/ChimeraCoder/anaconda"
	"net/url"
	"unsafe"
)

type relationFunc func (v url.Values) (c anaconda.UserCursor, err error)

const (
	relationFriend  = iota
	relationFollower
)

func (c *Connection) getAllRelationsFromX(relationType int, v url.Values) (Users, error){
	client := c.Client
	followerFunc := client.GetFollowersList
	friendFunc   := client.GetFriendsList
	relFunc := followerFunc
	if relationType == relationFriend{
		relFunc = friendFunc
	}

	nextCursor := int64(-1)
	nexCursorStr := fmt.Sprintf("%v", nextCursor)
	v.Set("cursor", nexCursorStr )
	allUsers := make([]User, 0)
	for {
		userCursor, err := relFunc(v)
		if err != nil{
			return allUsers, nil
		}
		nextCursor = userCursor.Next_cursor
		nextCursorStr := fmt.Sprintf("%v", nextCursor)
		v.Set("cursor", nextCursorStr)
		curUsers := *(*Users)(unsafe.Pointer(&userCursor.Users))
		allUsers = append(allUsers, curUsers...)
		if nextCursor == 0{
			break
		}
	}
	return allUsers, nil
}

func (c *Connection) getAllFriendsFromX(v url.Values) (Users, error){
	return c.getAllRelationsFromX(relationFriend, v)
}

func (c *Connection) GetAllFriendsFromScreenName(screenName string)(Users, error){
	v := url.Values{}
	v.Set("screen_name", screenName)
	v.Set("count", "200")
	return c.getAllFriendsFromX(v)
}

func (c *Connection) GetAllFriendsFromID(userID string)(Users,error){
	v := url.Values{}
	v.Set("user_id", userID)
	v.Set("count", "200")
	return c.getAllFriendsFromX(v)
}

func (c *Connection) getAllFollowersFromX(v url.Values) (Users, error){
	return c.getAllRelationsFromX(relationFollower, v)
}

func (c *Connection) GetAllFollowersFromScreenName(screenName string)(Users, error){
	v := url.Values{}
	v.Set("screen_name", screenName)
	v.Set("count", "200")
	return c.getAllFollowersFromX(v)
}

func (c *Connection) GetAllFollowersFromID(userID string)(Users,error){
	v := url.Values{}
	v.Set("user_id", userID)
	v.Set("count", "200")
	return c.getAllFollowersFromX(v)
}





